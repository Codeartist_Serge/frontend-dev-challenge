angular.module('ca_gitApp').component('cagitList', {
	templateUrl: 'js/ca_angular/templates/gitlist.html',
	controller: function cagitListController($http) {
		_this = this;
		this.ca_loading = true;
		this.messageFormat = function(msg) {
			msg = msg.split("\n\n");
			return msg[0];
		}
		this.dateFormat = function(dat) {
			var d = new Date(dat);
			var now = new Date();
			d = now - d;
			var days = Math.round(d/(1000*60*60*24));
			var hours = Math.round(d/(1000*60*60));
			var mins = Math.round(d/(1000*60));
			if(days == 1) return 'a day ago';
			if(days > 1) return days + ' days ago';
			if(hours == 1) return 'an hour ago';
			if(hours > 1) return hours + ' hours ago';
			if(mins == 1) return 'just now';
			if(mins > 1) return mins + ' minutes ago';
			return 'just now';
		}
		this.bgColoring = function(hash) {
			var c = hash.slice(-1);
			var nums = ['0','1','2','3','4','5','6','7','8','9'];
			if(nums.indexOf(c) >= 0) return "'#e6f1f6'";
			return "'#ffffff'";
		}
		this.refreshList = function() {
			_this.ca_gits = null;
			_this.ca_loading = true;
			$http.get('https://api.github.com/repos/angular/angular.js/commits?sha=master').then(function(response) {
				_this.ca_gits = response.data;
				_this.ca_loading = false;
			});
		}
		this.loadingBox = function() {
			if(_this.ca_loading) return 'ca_loading_visible';
			else return 'ca_loading_hidden';
		}
		$http.get('https://api.github.com/repos/angular/angular.js/commits?sha=master').then(function(response) {
			_this.ca_gits = response.data;
			_this.ca_loading = false;
		});
	}
});